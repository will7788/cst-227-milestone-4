﻿//Cell class that extends button, William Thornton, CST-227, Milestone-4, Coded by William Thornton on 08/17/2019
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_227_Milestone_4
{
    public class Cell : Button
    {
        private int counter = 0;
        protected override Size DefaultSize
        {
            get
            {
                return new Size(40, 40);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            counter++;
            this.Text = counter.ToString();
        }

    }
}
