﻿//Main minesweeper grid form, William Thornton, CST-227, Milestone-4, Coded by William Thornton on 08/17/2019
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_227_Milestone_4
{
    public partial class Minesweeper : Form
    {
        Cell[,] buttons;
        
        public Minesweeper()
        {
            //create popup dialog before game starts to check difficulty
            SetDifficulty diff = new SetDifficulty();
            diff.ShowDialog();
            InitializeComponent();
            //get the public difficulty variable to set the grid size, later it will add more live tiles as a ratio
            buttons = new Cell[diff.difficulty, diff.difficulty];
            this.Size = new Size((buttons.GetUpperBound(0) + 1) * 42, (buttons.GetUpperBound(buttons.Rank - 1) + 1) * 44);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
            CreateButtons(buttons);
        }

        public void CreateButtons(Cell[,] buttons)
        {
            //assign rows and columns based on the bounds of the array we defined
            int rowNumber = buttons.GetUpperBound(0) + 1;
            int colNumber = buttons.GetUpperBound(buttons.Rank - 1) + 1;

            for (int i = 0; i < rowNumber; i++)
            {
                for (int j = 0; j < colNumber; j++)
                {
                    buttons[i, j] = new Cell();
                    buttons[i, j].Location = new Point(j * 40, i * 40);
                    Controls.Add(buttons[i, j]);
                }
            }
        }
    }
}
